﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Security.Cryptography;

namespace CMofIS12_WF_Part2
{
    public partial class Lab9 : Form
    {
        public Lab9()
        {
            InitializeComponent();
        }
        long[] nums = new long[] { 65713, 69313, 73009, 76801, 84673, 16769023, 1073676287, 68718952447, 274876858367, 4398042316799 };//274876858367
        BigInteger var_p;
        BigInteger var_q;
        long var_d;
        ulong var_phi;
        private void generator_Click(object sender, EventArgs e) // генерация входных данных
        {
            Random rnd = new Random();
            // генерируем p и q так чтобы они небыли равны
            var_p = nums[rnd.Next(0, nums.Count() - 1)];
            do
                var_q = nums[rnd.Next(0, nums.Count() - 1)];
            while (var_q == var_p);

            var_phi = (ulong)(var_p - 1) * (ulong)(var_q - 1);

            variable_p.Text = (var_p).ToString();
            variable_q.Text = (var_q).ToString();

            // генерируем d (взаимно простое с phi и не равное p и q)
            do
            {
                var_d = nums[rnd.Next(0, nums.Count() - 1)];
            } while (var_d > (var_p * var_q) || var_d == var_p || var_d == var_q || Gcd(var_d, var_phi, out BigInteger outX, out BigInteger outY) != 1); // генерировать d до тех пор пока оно больше n, не равно p и q, и НОД (d; m) не будет равен 1

            variable_d.Text = var_d.ToString();
        }
        private void buttonEncrypt_Click(object sender, EventArgs e)
        {
            var hash = GetHash(textBox1.Text);
            try
            {
                var_p = Convert.ToInt64(variable_p.Text);
                var_q = Convert.ToInt64(variable_q.Text);
                var_d = Convert.ToInt64(variable_d.Text);
                if (var_p == var_q || var_p == var_d || var_q == var_d)
                    throw new Exception();
            }
            catch (Exception)
            {
                MessageBox.Show("Неверный тип введённых данных!");
                return;
            }

            //if (testSimple(var_p) == false || testSimple(var_q) == false || testSimple(var_d) == false)
            //{
            //    MessageBox.Show("Введенные числа должны быть простыми!");
            //    return;
            //}

            BigInteger n = var_p * var_q;
            BigInteger phi = (var_p - 1) * (var_q - 1);

            if (var_d < 2 || var_d > n)
            {
                MessageBox.Show("'d' должно быть больше 1 и меньше " + n.ToString());
                return;
            }

            BigInteger c;
            if (Gcd(var_d, phi, out BigInteger outX, out BigInteger outY) == 1)
                c = (outX % phi + phi) % phi; // инверсия (мультипликативно обратное к числу d)
            else
            {
                MessageBox.Show("'c' должно быть взаимно простым с " + phi);
                return;
            }
            textBox2.Text = textBox1.Text + ";" + calc(hash, c, n);

        }
        private void buttonTest_Click(object sender, EventArgs e) // проверка
        {
            string[] massA = textBox2.Text.Split(';');
            var hash = GetHash(massA[0]); // получаеи хег сообщения
            BigInteger n = Convert.ToInt64(variable_p.Text) * Convert.ToInt64(variable_q.Text); // n = p * q
            var w = calc(Convert.ToInt64(massA[1]), Convert.ToInt64(variable_d.Text), n); // w = hash(message)^d mod n
            textBox3.Text = "hash(Message) = " + hash + "  s^d mod N = " + w;
        }
        public BigInteger GetHash(string input)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            BigInteger result=0;
            foreach (var item in hash)
            {
                result += item;
            }
            return result;//BitConverter.ToString(hash).Replace("-", ""); ;
        }
        BigInteger calc(BigInteger a, BigInteger x, BigInteger p) // (a^x) mod p
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                var asd = Convert.ToUInt32(bit[i]);
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }
        BigInteger Gcd(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (b < a)
            {
                var t = a;
                a = b;
                b = t;
            }

            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            BigInteger gcd = Gcd(b % a, a, out x, out y);

            BigInteger newY = x;
            BigInteger newX = y - (b / a) * x;

            x = newX;
            y = newY;
            return gcd;
        }
        //bool testSimple(BigInteger num) // проверка числа на простоту
        //{
            
        //    //for (long i = 2; i <= Math.Sqrt(num); i++)
        //    for (long i = 2; i <= (Math.Exp(BigInteger.Log(num) / 2); i++)
        //        if (num % i == 0)
        //            return false;
        //    return true;
        //}
    }
}
