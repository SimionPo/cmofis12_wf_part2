﻿namespace CMofIS12_WF_Part2
{
    partial class Lab3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lab3));
            this.inputGenerator = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.a = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.variable_p = new System.Windows.Forms.TextBox();
            this.variable_q = new System.Windows.Forms.TextBox();
            this.variable_a = new System.Windows.Forms.TextBox();
            this.variable_b = new System.Windows.Forms.TextBox();
            this.help = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.labelA = new System.Windows.Forms.Label();
            this.labelKA = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelKB = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.variable_g = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // inputGenerator
            // 
            this.inputGenerator.Location = new System.Drawing.Point(213, 26);
            this.inputGenerator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.inputGenerator.Name = "inputGenerator";
            this.inputGenerator.Size = new System.Drawing.Size(125, 58);
            this.inputGenerator.TabIndex = 5;
            this.inputGenerator.Text = "Сгенерировать вх-е данные";
            this.inputGenerator.UseVisualStyleBackColor = true;
            this.inputGenerator.Click += new System.EventHandler(this.inputGenerator_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "p:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "q:";
            // 
            // a
            // 
            this.a.AutoSize = true;
            this.a.Location = new System.Drawing.Point(96, 30);
            this.a.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.a.Name = "a";
            this.a.Size = new System.Drawing.Size(20, 17);
            this.a.TabIndex = 3;
            this.a.Text = "a:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(96, 62);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "b:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 135);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(488, 68);
            this.label5.TabIndex = 5;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Location = new System.Drawing.Point(123, 231);
            this.labelB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(13, 17);
            this.labelB.TabIndex = 6;
            this.labelB.Text = "-";
            // 
            // variable_p
            // 
            this.variable_p.Enabled = false;
            this.variable_p.Location = new System.Drawing.Point(33, 26);
            this.variable_p.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.variable_p.Name = "variable_p";
            this.variable_p.Size = new System.Drawing.Size(53, 22);
            this.variable_p.TabIndex = 1;
            this.variable_p.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.variable_p_KeyPress);
            // 
            // variable_q
            // 
            this.variable_q.Location = new System.Drawing.Point(33, 59);
            this.variable_q.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.variable_q.Name = "variable_q";
            this.variable_q.Size = new System.Drawing.Size(53, 22);
            this.variable_q.TabIndex = 2;
            this.variable_q.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.variable_q_KeyPress);
            // 
            // variable_a
            // 
            this.variable_a.Location = new System.Drawing.Point(125, 26);
            this.variable_a.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.variable_a.Name = "variable_a";
            this.variable_a.Size = new System.Drawing.Size(53, 22);
            this.variable_a.TabIndex = 3;
            this.variable_a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.variable_a_KeyPress);
            // 
            // variable_b
            // 
            this.variable_b.Location = new System.Drawing.Point(125, 58);
            this.variable_b.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.variable_b.Name = "variable_b";
            this.variable_b.Size = new System.Drawing.Size(53, 22);
            this.variable_b.TabIndex = 4;
            this.variable_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.variable_b_KeyPress);
            // 
            // help
            // 
            this.help.Location = new System.Drawing.Point(347, 26);
            this.help.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(61, 58);
            this.help.TabIndex = 6;
            this.help.Text = "help";
            this.help.UseVisualStyleBackColor = true;
            this.help.Click += new System.EventHandler(this.help_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(213, 198);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(195, 96);
            this.button1.TabIndex = 7;
            this.button1.Text = "Рассчет";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(123, 210);
            this.labelA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(13, 17);
            this.labelA.TabIndex = 13;
            this.labelA.Text = "-";
            // 
            // labelKA
            // 
            this.labelKA.AutoSize = true;
            this.labelKA.Location = new System.Drawing.Point(173, 254);
            this.labelKA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelKA.Name = "labelKA";
            this.labelKA.Size = new System.Drawing.Size(13, 17);
            this.labelKA.TabIndex = 14;
            this.labelKA.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 210);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "A = q^a mod p =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 231);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "B = q^b mod p =";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 254);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "K for Alice = B^a mod p =";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 278);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(162, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "K for Bob = A^b mod p =";
            // 
            // labelKB
            // 
            this.labelKB.AutoSize = true;
            this.labelKB.Location = new System.Drawing.Point(173, 278);
            this.labelKB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelKB.Name = "labelKB";
            this.labelKB.Size = new System.Drawing.Size(13, 17);
            this.labelKB.TabIndex = 19;
            this.labelKB.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 348);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(488, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "---------------------------------------------------------------------------------" +
    "---------------\r\n";
            // 
            // variable_g
            // 
            this.variable_g.Location = new System.Drawing.Point(33, 94);
            this.variable_g.Margin = new System.Windows.Forms.Padding(4);
            this.variable_g.Name = "variable_g";
            this.variable_g.Size = new System.Drawing.Size(53, 22);
            this.variable_g.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 94);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "g:";
            // 
            // Lab3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 519);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.variable_g);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelKB);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelKA);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.help);
            this.Controls.Add(this.variable_b);
            this.Controls.Add(this.variable_a);
            this.Controls.Add(this.variable_q);
            this.Controls.Add(this.variable_p);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.a);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputGenerator);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Lab3";
            this.Text = "Алгоритм Диффи-Хеллмана";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button inputGenerator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label a;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.TextBox variable_p;
        private System.Windows.Forms.TextBox variable_q;
        private System.Windows.Forms.TextBox variable_a;
        private System.Windows.Forms.TextBox variable_b;
        private System.Windows.Forms.Button help;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelKA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelKB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox variable_g;
        private System.Windows.Forms.Label label10;
    }
}