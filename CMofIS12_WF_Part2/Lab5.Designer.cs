﻿namespace CMofIS12_WF_Part2
{
    partial class Lab5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputGenerator = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.variable_p = new System.Windows.Forms.TextBox();
            this.variable_g = new System.Windows.Forms.TextBox();
            this.variable_cAdA = new System.Windows.Forms.TextBox();
            this.variable_cBdB = new System.Windows.Forms.TextBox();
            this.message = new System.Windows.Forms.TextBox();
            this.encryptText = new System.Windows.Forms.TextBox();
            this.textResult = new System.Windows.Forms.TextBox();
            this.begin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pShip = new System.Windows.Forms.Label();
            this.gShip = new System.Windows.Forms.Label();
            this.labelK = new System.Windows.Forms.Label();
            this.textLang = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.take = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputGenerator
            // 
            this.inputGenerator.BackColor = System.Drawing.Color.GhostWhite;
            this.inputGenerator.Location = new System.Drawing.Point(10, 219);
            this.inputGenerator.Name = "inputGenerator";
            this.inputGenerator.Size = new System.Drawing.Size(498, 23);
            this.inputGenerator.TabIndex = 0;
            this.inputGenerator.Text = "Сгенерировать входные данные!";
            this.inputGenerator.UseVisualStyleBackColor = false;
            this.inputGenerator.Click += new System.EventHandler(this.inputGenerator_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "p:";
            // 
            // variable_p
            // 
            this.variable_p.Location = new System.Drawing.Point(35, 121);
            this.variable_p.Name = "variable_p";
            this.variable_p.Size = new System.Drawing.Size(119, 22);
            this.variable_p.TabIndex = 2;
            // 
            // variable_g
            // 
            this.variable_g.Location = new System.Drawing.Point(35, 150);
            this.variable_g.Name = "variable_g";
            this.variable_g.Size = new System.Drawing.Size(119, 22);
            this.variable_g.TabIndex = 3;
            // 
            // variable_cAdA
            // 
            this.variable_cAdA.Location = new System.Drawing.Point(68, 193);
            this.variable_cAdA.Name = "variable_cAdA";
            this.variable_cAdA.Size = new System.Drawing.Size(142, 22);
            this.variable_cAdA.TabIndex = 4;
            // 
            // variable_cBdB
            // 
            this.variable_cBdB.Location = new System.Drawing.Point(368, 193);
            this.variable_cBdB.Name = "variable_cBdB";
            this.variable_cBdB.Size = new System.Drawing.Size(140, 22);
            this.variable_cBdB.TabIndex = 5;
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(79, 248);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(429, 22);
            this.message.TabIndex = 6;
            // 
            // encryptText
            // 
            this.encryptText.Location = new System.Drawing.Point(10, 305);
            this.encryptText.Multiline = true;
            this.encryptText.Name = "encryptText";
            this.encryptText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.encryptText.Size = new System.Drawing.Size(498, 22);
            this.encryptText.TabIndex = 7;
            // 
            // textResult
            // 
            this.textResult.Location = new System.Drawing.Point(79, 366);
            this.textResult.Name = "textResult";
            this.textResult.Size = new System.Drawing.Size(429, 22);
            this.textResult.TabIndex = 8;
            // 
            // begin
            // 
            this.begin.BackColor = System.Drawing.Color.GhostWhite;
            this.begin.Location = new System.Drawing.Point(433, 276);
            this.begin.Name = "begin";
            this.begin.Size = new System.Drawing.Size(75, 26);
            this.begin.TabIndex = 9;
            this.begin.Text = "begin";
            this.begin.UseVisualStyleBackColor = false;
            this.begin.Click += new System.EventHandler(this.begin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "g:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "cA;dA:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(313, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "cB;dB:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(78, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "ALICE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(387, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "BOB";
            // 
            // pShip
            // 
            this.pShip.AutoSize = true;
            this.pShip.Location = new System.Drawing.Point(203, 124);
            this.pShip.Name = "pShip";
            this.pShip.Size = new System.Drawing.Size(126, 17);
            this.pShip.TabIndex = 15;
            this.pShip.Text = "---------------------->";
            // 
            // gShip
            // 
            this.gShip.AutoSize = true;
            this.gShip.Location = new System.Drawing.Point(203, 153);
            this.gShip.Name = "gShip";
            this.gShip.Size = new System.Drawing.Size(126, 17);
            this.gShip.TabIndex = 16;
            this.gShip.Text = "---------------------->";
            // 
            // labelK
            // 
            this.labelK.AutoSize = true;
            this.labelK.Location = new System.Drawing.Point(203, 276);
            this.labelK.Name = "labelK";
            this.labelK.Size = new System.Drawing.Size(32, 17);
            this.labelK.TabIndex = 17;
            this.labelK.Text = "___";
            // 
            // textLang
            // 
            this.textLang.Location = new System.Drawing.Point(12, 12);
            this.textLang.Multiline = true;
            this.textLang.Name = "textLang";
            this.textLang.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textLang.Size = new System.Drawing.Size(498, 71);
            this.textLang.TabIndex = 21;
            this.textLang.Text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "Message:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 285);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 17);
            this.label8.TabIndex = 23;
            this.label8.Text = "Code:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "Message:";
            // 
            // take
            // 
            this.take.BackColor = System.Drawing.Color.GhostWhite;
            this.take.Location = new System.Drawing.Point(433, 333);
            this.take.Name = "take";
            this.take.Size = new System.Drawing.Size(75, 26);
            this.take.TabIndex = 25;
            this.take.Text = "take";
            this.take.UseVisualStyleBackColor = false;
            this.take.Click += new System.EventHandler(this.take_Click);
            // 
            // Lab5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 531);
            this.Controls.Add(this.take);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textLang);
            this.Controls.Add(this.labelK);
            this.Controls.Add(this.gShip);
            this.Controls.Add(this.pShip);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.begin);
            this.Controls.Add(this.textResult);
            this.Controls.Add(this.encryptText);
            this.Controls.Add(this.message);
            this.Controls.Add(this.variable_cBdB);
            this.Controls.Add(this.variable_cAdA);
            this.Controls.Add(this.variable_g);
            this.Controls.Add(this.variable_p);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputGenerator);
            this.Name = "Lab5";
            this.Text = "Шифр Эль-Гамаля";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button inputGenerator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox variable_p;
        private System.Windows.Forms.TextBox variable_g;
        private System.Windows.Forms.TextBox variable_cAdA;
        private System.Windows.Forms.TextBox variable_cBdB;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.TextBox encryptText;
        private System.Windows.Forms.TextBox textResult;
        private System.Windows.Forms.Button begin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label pShip;
        private System.Windows.Forms.Label gShip;
        private System.Windows.Forms.Label labelK;
        private System.Windows.Forms.TextBox textLang;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button take;
    }
}