﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Collections;

namespace CMofIS12_WF_Part2
{
    public partial class Lab6 : Form
    {
        public int sizeOfBlock { get; set; } = 512; // размер блока 512 бит
        public int sizeOfWord { get; set; } = 32; // размер блока 512 бит
        private const int sizeOfChar = 8; //размер одного символа (in Unicode 16 bit)
        string[] blocks; //сами блоки в двоичном формате
        // шаг 3. инициализация буфера
        string A, B, C, D, a_, b_, c_, d_ = "";
        private void Initialization()
        {
            a_ = "01100111010001010010001100000001"; // 67452301
            b_ = "11101111110011011010101110001001"; // efcdab89
            c_ = "10011000101110101101110011111110"; // 98badcfe
            d_ = "00010000001100100101010001110110"; // 10325476
        }

        int[] s = new int[] { 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21 };
        public Lab6()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Initialization();
            byte[] input = Encoding.UTF8.GetBytes(textBox1.Text);
            //BitArray ba = new BitArray(input);
            //byte[] b = new byte[3];
            //ba.CopyTo(b, 0);

            // переводим байты ключа в бинарный формат
            string inputBits = "";
            for (int i = 0; i < input.Count(); i++)
                inputBits += ByteToBinaryFormat(input[i]);

            // шаг 1. выравнивание потока
            inputBits = AppendPaddingBits(inputBits); // добавить биты дополнения
            // шаг 2. добавление длины сообщения
            inputBits = AppendLength(inputBits, input.Count() * 8);
            CutBitIntoBlocks(inputBits); // разбиение строки бит на блоки
            // шаг 3. инициализация буфера

            // шаг 4. вычисление в цикле
            for (int i = 0; i < blocks.Count(); i++)
            {
                A = a_;
                B = b_;
                C = c_;
                D = d_;
                string[] massBlock = CutBlock(blocks[i]); // массив из 16 слов по 32 бита.
                var time = "";
                int g = 0;
                // 4 цикла
                for (int j = 0; j < 64; j++)
                {
                    if (j >= 0 && j <= 15)
                    {
                        time = FunF(B, C, D);
                        g = j;
                    }
                    else if (j >= 16 && j <= 31)
                    {
                        time = FunG(B, C, D);
                        g = ((5 * j) + 1) % 16;
                    }
                    else if (j >= 32 && j <= 47)
                    {
                        time = FunH(B, C, D);
                        g = ((3 * j) + 5) % 16;
                    }
                    else if (j >= 48 && j <= 63)
                    {
                        time = FunI(B, C, D);
                        g = (7 * j) % 16;
                    }

                    time = f((f(time, A, 32)), massBlock[g], 32);
                    time = f(time, ConstantTable(j), 32);

                    A = D;
                    D = C;
                    C = B;
                    B = f(B, (ShiftLeft(time, s[j])), 32);
                }

                a_ = f(A, a_, 32);
                b_ = f(B, b_, 32);
                c_ = f(C, c_, 32);
                d_ = f(D, d_, 32);
            }
            // шаг 5. результат вычислений
            byte[] result = new byte[0];
            result = StringFromBinaryToNormalFormat(result, CoupByte(a_) + CoupByte(b_) + CoupByte(c_) + CoupByte(d_));
            textBox2.Text = BitConverter.ToString(result).Replace("-", "");
        }
        // смена порядка байт, в строке, на противоположный
        private string CoupByte(string input)
        {
            string res = "";
            for (int i = 0; i < input.Length / 8; i++)
                res += input.Substring(input.Length - 8 * (i + 1), 8);
            return res;
        }
        // перевод байта в двоичный формат
        private string ByteToBinaryFormat(byte input)
        {
            string output = "";

            string char_binary = Convert.ToString(input, 2);

            while (char_binary.Length < sizeOfChar)
                char_binary = "0" + char_binary;

            output += char_binary;
            return output;
        }
        // добавление нулевых бит чтобы новая длина потока стала сравнима с 448 по модулю 512
        private string AppendPaddingBits(string input)
        {
            input += "1"; // сначала к концу потока дописывают единичный бит.
            while ((input.Length % 512) != 448) // затем добавляют некоторое число нулевых бит
                input += "0";

            return input;
        }
        // добавление длины сообщения
        private string AppendLength(string input, int count)
        {
            string countToBit = Convert.ToString(count, 2);
            while ((countToBit.Length < 64)) countToBit = "0" + countToBit; // добавляем нули, если длинна меньше 64бит
            countToBit = countToBit.Substring(56, 8) + countToBit.Substring(48, 8) + countToBit.Substring(40, 8) + countToBit.Substring(32, 8) + countToBit.Substring(0, 32);
            if (countToBit.Length == 64) return input + countToBit;//countToBit.Substring(countToBit.Length / 2) + countToBit.Substring(0, countToBit.Length / 2); // cначала записывают младшие 4 байта, затем старшие
            if (countToBit.Length > 64) countToBit = countToBit.Substring(countToBit.Length - 64); // если длина превосходит 64 то дописывают только младшие биты
            return input + countToBit;
        }
        // разбиение обычной строки на блоки
        private void CutBitIntoBlocks(string input)
        {
            blocks = new string[input.Length / sizeOfBlock];
            for (int i = 0; i < blocks.Count(); i++)
            {
                blocks[i] = input.Substring(0, sizeOfBlock);
                input = input.Substring(sizeOfBlock);
            }
        }
        // разбиение блока на 32х разрядные слова
        private string[] CutBlock(string input)
        {
            string[] block = new string[input.Length / sizeOfWord];
            for (int i = 0; i < block.Count(); i++)
            {
                block[i] = CoupByte(input.Substring(0, sizeOfWord));
                input = input.Substring(sizeOfWord);
            }
            return block;
        }
        // таблица констант
        private string ConstantTable(double n)
        {
            n += 1;
            // n = (n * Math.PI) / 180.0; // перевод градусов в радианы 3.14159265368979323846264338
            n = 4294967296 * (Math.Abs(Math.Sin(n))); // 2^32 * |sin n|
            n = Math.Floor(n);

            string T = Convert.ToString(Convert.ToInt64(n), 2); // перевод в 32x битное число
            while (T.Length < 32)
                T = "0" + T;
            return T;
        }
        // циклический сдвиг влево на n эл-в
        private string ShiftLeft(string word, int n)
        {
            return word.Substring(n) + word.Substring(0, n);
        }
        private void ShiftABCD()
        {
            var timeA = A;
            A = D;
            D = C;
            C = B;
            B = timeA;
        }
        // сложение по модулю 2^32
        private string f(string s1, string s2, int mod)
        {
            string result = "";
            int time = 0;
            //int j = s2.Length-1;
            for (int i = s1.Length - 1; i > -1; i--)
            {
                int a = s1[i] - 48;
                int b = s2[i] - 48;
                if (a + b + time == 0)
                {
                    time = 0;
                    result = "0" + result;
                }
                else if (a + b + time == 1)
                {
                    time = 0;
                    result = "1" + result;
                }
                else if (a + b + time == 2)
                {
                    time = 1;
                    result = "0" + result;
                }
                else if (a + b + time == 3)
                {
                    time = 1;
                    result = "1" + result;
                }
                if (i == 0 && time == 1)
                    result = "1" + result;
                //j--;
            }
            if (result.Substring(result.Length - mod).Length == mod)
                return result.Substring(result.Length - mod);
            else return "error";
        }
        // логические функции
        string FunF(string B, string C, string D)
        {
            return OR(AND(B, C), AND(NOT(B), D)); //
        }
        string FunG(string B, string C, string D)
        {
            return OR(AND(B, D), AND(C, NOT(D)));
        }
        string FunH(string B, string C, string D)
        {
            return XOR(XOR(B, C), D);
        }
        string FunI(string B, string C, string D)
        {
            return XOR(C, (OR(B, NOT(D))));
        }
        // переводим строку с двоичными данными в байтовый массив
        private byte[] StringFromBinaryToNormalFormat(byte[] result, string input)
        {
            while (input.Length > 0)
            {
                string char_binary = input.Substring(0, sizeOfChar);
                input = input.Remove(0, sizeOfChar);

                int a = 0;
                int degree = char_binary.Length - 1;

                foreach (char c in char_binary)
                    a += Convert.ToInt32(c.ToString()) * (int)Math.Pow(2, degree--);

                Array.Resize(ref result, result.Count() + 1);// изменяем размер
                result[result.Count() - 1] = (byte)a;
            }

            return result;
        }
        //XOR двух строк с двоичными данными
        private string XOR(string s1, string s2)
        {
            string result = "";

            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a ^ b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        //AND двух строк с двоичными данными
        private string AND(string s1, string s2)
        {
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a & b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        //OR двух строк с двоичными данными
        private string OR(string s1, string s2)
        {
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a | b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        //NOT строки
        private string NOT(string s1)
        {
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                if (s1[i] == '1') result += "0";
                if (s1[i] == '0') result += "1";
            }
            return result;
        }
    }
}
