﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMofIS12_WF_Part2
{
    public partial class Lab8 : Form
    {
        public Lab8()
        {
            InitializeComponent();
        }
        public int sizeOfBlock { get; set; } = 256; // размер блока 256 бит
        private const int sizeOfChar = 8; // размер одного символа (in Unicode 16 bit)
        string[] blocks; // сами блоки в двоичном формате
        string[] keys = new string[4]; // 4 ключа
        static string zero = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
                             "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" +
                             "0000000000000000000000000000000000000000000000000000000000000000000000";
        static string C3 = "11111111000000001111111111111111000000000000000000000000111111111111111100000000000000001111111" +
                           "10000000011111111111111110000000000000000111111110000000011111111000000001111111100000000111111" +
                           "111111111100000000111111110000000011111111000000001111111100000000";

        // «Тестовый» набор S-блоков from wikipedia
        int[,] SBloks = {
        {4,  10,  9,   2,   13,  8,   0,   14,  6,   11,  1,   12,  7,   15,  5,   3 },
        {14, 11,  4,   12,  6,   13,  15,  10,  2,   3,   8,   1,   0,   7,   5,   9 },
        {5,  8,   1,   13,  10,  3,   4,   2,   14,  15,  12,  7,   6,   0,   9,   11 },
        {7,  13,  10,  1,   0,   8,   9,   15,  14,  4,   6,   12,  11,  2,   5,   3 },
        {6,  12,  7,   1,   5,   15,  13,  8,   4,   10,  9,   14,  0,   3,   11,  2 },
        {4,  11,  10,  0,   7,   2,   1,   13,  3,   6,   8,   5,   9,   12,  15,  14 },
        {13, 11,  4,   1,   3,   15,  5,   9,   0,   10,  14,  7,   6,   8,   2,   12 },
        {1,  15,  13,  0,   5,   7,   10,  4,   9,   2,   3,   14,  6,   11,  8,   12 }
        };
        string H1 = zero;
        string ControlSumm = zero;
        string Leight = zero;
        string[] cosntantC = new string[4] { "not use", zero, C3, zero }; // константы 'C'

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = GOST_R_34_11_94(textBox1.Text);
        }
        // криптографический стандарт вычисления хеш-функции
        string GOST_R_34_11_94(string inputText)
        {
            byte[] input = Encoding.UTF8.GetBytes(inputText).Reverse().ToArray(); // биты сообщения в Little-endian
            string inputBits = "";
            // перевод байт в строку бит
            for (int i = 0; i < input.Count(); i++)
                inputBits += ByteToBinaryFormat(input[i]);

            inputBits = AppendPaddingBits(inputBits); // добавить биты дополнения
            CutBitIntoBlocks(inputBits); // разбиение строки бит на блоки по 256бит

            // контрольная сумма сообщения
            try
            {
                ControlSumm = blocks[0];
            }
            catch (Exception)
            {
                blocks = new string[1];
                blocks[0] = zero;
            }
            for (int i = 1; i < blocks.Count(); i++)
                ControlSumm = f(ControlSumm, blocks[i], 256);

            // длина сообщения в битах по модулю  2^256
            Leight = Convert.ToString(input.Length * 8, 2);
            while (Leight.Length < 256) Leight = "0" + Leight;
            

            var S = ""; 
            var H2 = "";
            var H3 = "";
            var H4 = "";
            for (int i = 0; i < blocks.Count(); i++)
            {
                // вычисляем H2
                GetKey(blocks[i], H1); // генерация ключей
                string s1 = E_GOST(H1.Substring(192, 64), keys[0]);
                string s2 = E_GOST(H1.Substring(128, 64), keys[1]);
                string s3 = E_GOST(H1.Substring(64, 64), keys[2]);
                string s4 = E_GOST(H1.Substring(0, 64), keys[3]);
                S = s4 + s3 + s2 + s1;
                H2 = MixingTransformation(S, blocks[i], H1);
                // вычисляем H3
                GetKey(Leight, H2); // генерация ключей
                s1 = E_GOST(H2.Substring(192, 64), keys[0]);
                s2 = E_GOST(H2.Substring(128, 64), keys[1]);
                s3 = E_GOST(H2.Substring(64, 64), keys[2]);
                s4 = E_GOST(H2.Substring(0, 64), keys[3]);
                S = s4 + s3 + s2 + s1; // 1100110101101110000111101011110
                H3 = MixingTransformation(S, Leight, H2);
                // вычисляем H4
                GetKey(ControlSumm, H3); // генерация ключей
                s1 = E_GOST(H3.Substring(192, 64), keys[0]);
                s2 = E_GOST(H3.Substring(128, 64), keys[1]);
                s3 = E_GOST(H3.Substring(64, 64), keys[2]);
                s4 = E_GOST(H3.Substring(0, 64), keys[3]);
                S = s4 + s3 + s2 + s1; 
                H4 = MixingTransformation(S, ControlSumm, H3);
                // var b = BitConverter.ToString(input).Replace("-", "");
            }
            // результат вычислений
            byte[] result = new byte[0];
            result = StringFromBinaryToNormalFormat(result, H4).Reverse().ToArray();
            return BitConverter.ToString(result).Replace("-", "").ToLower();
            
        }
        // генерация ключей
        private void GetKey(string inputMess, string Hin)
        {
            var U = "";
            var V = "";
            var W = "";
            for (int j = 0; j < 4; j++)
            {
                if (j == 0)
                {
                    U = Hin;
                    V = inputMess;
                    W = XOR(U, V);
                    keys[j] = P(W);
                }
                else
                {
                    U = XOR(A(U), cosntantC[j]);
                    V = A(A(V));
                    W = XOR(U, V);
                    keys[j] = P(W);
                }
            }
        }
        // ф-я для генерации ключей (keys)
        private string A(string input)
        {
            string y4 = input.Substring(0, 64);
            string y3 = input.Substring(64, 64);
            string y2 = input.Substring(128, 64);
            string y1 = input.Substring(192, 64);

            return XOR(y1, y2) + y4 + y3 + y2;
        }
        // ф-я для генерации ключей (keys)
        private string P(string input)
        {
            string res = "";
            string[] k = new string[32];
            for (int i = 0; i < 32; i++) k[i] = input.Substring(i * 8, 8);
            for (int j = 0; j < 8; j++)
            {
                for (int i = 0; i < 4; i++)
                {
                    int index = (8 * i) + (j + 1);
                    res += k[index - 1];
                }
            }

            return res;
        }
        // шмфрование, ГОСТ_28147—89 Режим простой замены
        private string E_GOST(string message, string key) // message = 64 бита, key = 256 бит
        {
            string[] keyMass = new string[key.Length / 32];
            string B0 = message.Substring(0, message.Length / 2); // блок открытого текста сначала
            string A0 = message.Substring(message.Length / 2); // разбивается на две половины
            for (int i = 0, j = keyMass.Length - 1; i < keyMass.Length; i++) keyMass[j--] = key.Substring(i * 32, 32); // исходный 256-битный ключ разбивается на восемь 32-битных  // ошибка 1, была тут
            for (int i = 0; i < 32; i++) // реализация режима простой замены
            {
                var time = A0;
                if (i < 24)
                    A0 = XOR(B0, fun(A0, keyMass[i % 8]));
                else if (i >= 24)
                    A0 = XOR(B0, fun(A0, keyMass[31 - i]));
                B0 = time;
            }
            return A0 + B0;
        }
        // ф-я для шмфрования по ГОСТ_28147—89 (нужна для E_GOST)
        private string fun(string A, string X)
        {
            string xor = f(A, X, 32); // складываются по модулю 2^32
            string res = "";
            for (int i = 0; i < 8; i++) // обработка входного массива xor через массив S-блоков
            {
                int j = 0;
                var time = xor.Substring(4 * i, 4); // xor разбивается на восемь 4-битовых подпоследовательностей
                // перевод 4бит в 10ю систему
                int degreeJ = time.Length - 1;
                foreach (char c in time)
                    j += Convert.ToInt32(c.ToString()) * (int)Math.Pow(2, degreeJ--);
                // полученное число из S-блока переводим в 2ю систему в 4х битовое число
                time = Convert.ToString(SBloks[7 - i, j], 2); // ошибка 2, была тут
                while (time.Length < 4)
                    time = "0" + time;
                res += time;
            }

            return ShiftLeft(res, 11); // выходы всех восьми S-блоков циклически сдвигается влево на 11 битов.
        }
        // ф-я элементарного преобразования блока длиной 256 бит в блок той же длины
        private string Psi(string input)
        {
            string res = "";
            string[] y = new string[input.Length / 16];
            for (int i = 0, j = y.Count() - 1; i < y.Count(); i++, j--) y[j] = input.Substring(i * 16, 16); // исходный 256-битный разбивается на 16 16-битных
            res = y[0];
            for (int i = 1; i < 4; i++) res = XOR(res, y[i]);
            res = XOR(res, y[12]);
            res = XOR(res, y[15]);
            for (int i = y.Count() - 1; i > 0; i--) res += y[i];
            return res;
        }
        // Перемешивающее преобразование
        private string MixingTransformation(string S, string block, string Hinp)
        {

            for (int i = 0; i < 12; i++) S = Psi(S);
            S = XOR(block, S);
            S = Psi(S);
            S = XOR(Hinp, S);
            for (int l = 0; l < 61; l++) S = Psi(S);
            return S;
        }
        // перевод байта в двоичный формат
        private string ByteToBinaryFormat(byte input)
        {
            string output = "";

            string char_binary = Convert.ToString(input, 2);

            while (char_binary.Length < sizeOfChar)
                char_binary = "0" + char_binary;

            output += char_binary;
            return output;
        }
        // добавление нулевых бит чтобы новая длина потока стала сравнима с 0 по модулю 256
        private string AppendPaddingBits(string input)
        {
            int count = 256 - (input.Length % 256);
            if (count == 256) count = 0;
            string append = "";
            for (int i = 0; i < count; i++) append += "0";
            //input = input.Substring(0, input.Length - (input.Length % 256)) + append + input.Substring(input.Length - (input.Length % 256));
            input = append + input;
            return input;
        }
        // разбиение обычной строки на блоки
        private void CutBitIntoBlocks(string input)
        {
            blocks = new string[input.Length / sizeOfBlock];
            for (int i = 0; i < blocks.Count(); i++)
            {
                blocks[i] = input.Substring(0, sizeOfBlock);
                input = input.Substring(sizeOfBlock);
            }
        }
        // сложение по модулю 2^32
        private string f(string s1, string s2, int mod)
        {
            string result = "";
            int time = 0;
            //int j = s2.Length-1;
            for (int i = s1.Length - 1; i > -1; i--)
            {
                int a = s1[i] - 48;
                int b = s2[i] - 48;
                if (a + b + time == 0)
                {
                    time = 0;
                    result = "0" + result;
                }
                else if (a + b + time == 1)
                {
                    time = 0;
                    result = "1" + result;
                }
                else if (a + b + time == 2)
                {
                    time = 1;
                    result = "0" + result;
                }
                else if (a + b + time == 3)
                {
                    time = 1;
                    result = "1" + result;
                }
                if (i == 0 && time == 1)
                    result = "1" + result;
                //j--;
            }
            if (result.Substring(result.Length - mod).Length == mod)
                return result.Substring(result.Length - mod);
            else return "error";
        }
        // циклический сдвиг влево на n эл-в
        private string ShiftLeft(string word, int n)
        {
            return word.Substring(n % word.Length) + word.Substring(0, n % word.Length);
        }
        //XOR двух строк с двоичными данными
        private string XOR(string s1, string s2)
        {
            string result = "";

            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a ^ b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        // переводим строку с двоичными данными в байтовый массив
        private byte[] StringFromBinaryToNormalFormat(byte[] result, string input)
        {
            while (input.Length > 0)
            {
                string char_binary = input.Substring(0, sizeOfChar);
                input = input.Remove(0, sizeOfChar);

                int a = 0;
                int degree = char_binary.Length - 1;

                foreach (char c in char_binary)
                    a += Convert.ToInt32(c.ToString()) * (int)Math.Pow(2, degree--);

                Array.Resize(ref result, result.Count() + 1);// изменяем размер
                result[result.Count() - 1] = (byte)a;
            }

            return result;
        }
    }
}
