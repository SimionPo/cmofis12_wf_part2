﻿namespace CMofIS12_WF_Part2
{
    partial class Lab11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxTest = new System.Windows.Forms.TextBox();
            this.buttonTest = new System.Windows.Forms.Button();
            this.messageCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.begin = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.variable_xy = new System.Windows.Forms.TextBox();
            this.message = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.inputGenerator = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.variable_q = new System.Windows.Forms.TextBox();
            this.variable_p = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.variable_a = new System.Windows.Forms.TextBox();
            this.variable_x1y1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxTest
            // 
            this.textBoxTest.Location = new System.Drawing.Point(81, 206);
            this.textBoxTest.Name = "textBoxTest";
            this.textBoxTest.Size = new System.Drawing.Size(429, 22);
            this.textBoxTest.TabIndex = 49;
            // 
            // buttonTest
            // 
            this.buttonTest.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonTest.Location = new System.Drawing.Point(423, 277);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 26);
            this.buttonTest.TabIndex = 48;
            this.buttonTest.Text = "test";
            this.buttonTest.UseVisualStyleBackColor = false;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // messageCode
            // 
            this.messageCode.Location = new System.Drawing.Point(81, 175);
            this.messageCode.Name = "messageCode";
            this.messageCode.Size = new System.Drawing.Size(429, 22);
            this.messageCode.TabIndex = 47;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(372, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 17);
            this.label5.TabIndex = 46;
            this.label5.Text = "sicret key ; open key";
            // 
            // begin
            // 
            this.begin.BackColor = System.Drawing.Color.GhostWhite;
            this.begin.Location = new System.Drawing.Point(423, 245);
            this.begin.Name = "begin";
            this.begin.Size = new System.Drawing.Size(75, 26);
            this.begin.TabIndex = 45;
            this.begin.Text = "begin";
            this.begin.UseVisualStyleBackColor = false;
            this.begin.Click += new System.EventHandler(this.begin_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(340, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 17);
            this.label4.TabIndex = 44;
            this.label4.Text = "x;y:";
            // 
            // variable_xy
            // 
            this.variable_xy.Location = new System.Drawing.Point(375, 37);
            this.variable_xy.Name = "variable_xy";
            this.variable_xy.Size = new System.Drawing.Size(135, 22);
            this.variable_xy.TabIndex = 43;
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(81, 146);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(429, 22);
            this.message.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 17);
            this.label3.TabIndex = 41;
            this.label3.Text = "Test:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 17);
            this.label8.TabIndex = 40;
            this.label8.Text = "Code:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 39;
            this.label7.Text = "Message:";
            // 
            // inputGenerator
            // 
            this.inputGenerator.BackColor = System.Drawing.Color.GhostWhite;
            this.inputGenerator.Location = new System.Drawing.Point(12, 107);
            this.inputGenerator.Name = "inputGenerator";
            this.inputGenerator.Size = new System.Drawing.Size(498, 23);
            this.inputGenerator.TabIndex = 38;
            this.inputGenerator.Text = "Сгенерировать входные данные!";
            this.inputGenerator.UseVisualStyleBackColor = false;
            this.inputGenerator.Click += new System.EventHandler(this.inputGenerator_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 37;
            this.label2.Text = "q:";
            // 
            // variable_q
            // 
            this.variable_q.Location = new System.Drawing.Point(30, 41);
            this.variable_q.Name = "variable_q";
            this.variable_q.Size = new System.Drawing.Size(304, 22);
            this.variable_q.TabIndex = 36;
            // 
            // variable_p
            // 
            this.variable_p.Location = new System.Drawing.Point(30, 12);
            this.variable_p.Name = "variable_p";
            this.variable_p.Size = new System.Drawing.Size(304, 22);
            this.variable_p.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 34;
            this.label1.Text = "p:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 17);
            this.label6.TabIndex = 51;
            this.label6.Text = "a:";
            // 
            // variable_a
            // 
            this.variable_a.Location = new System.Drawing.Point(30, 69);
            this.variable_a.Name = "variable_a";
            this.variable_a.Size = new System.Drawing.Size(304, 22);
            this.variable_a.TabIndex = 50;
            // 
            // variable_x1y1
            // 
            this.variable_x1y1.Location = new System.Drawing.Point(375, 63);
            this.variable_x1y1.Name = "variable_x1y1";
            this.variable_x1y1.Size = new System.Drawing.Size(135, 22);
            this.variable_x1y1.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(340, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 17);
            this.label9.TabIndex = 53;
            this.label9.Text = "x;y:";
            // 
            // Lab11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 365);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.variable_x1y1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.variable_a);
            this.Controls.Add(this.textBoxTest);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.messageCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.begin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.variable_xy);
            this.Controls.Add(this.message);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.inputGenerator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.variable_q);
            this.Controls.Add(this.variable_p);
            this.Controls.Add(this.label1);
            this.Name = "Lab11";
            this.Text = "DSA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxTest;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.TextBox messageCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button begin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox variable_xy;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button inputGenerator;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox variable_q;
        private System.Windows.Forms.TextBox variable_p;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox variable_a;
        private System.Windows.Forms.TextBox variable_x1y1;
        private System.Windows.Forms.Label label9;
    }
}