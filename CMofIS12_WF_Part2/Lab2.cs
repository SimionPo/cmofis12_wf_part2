﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMofIS12_WF_Part2
{
    public partial class Lab2 : Form
    {
        public Lab2()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        string RUS = " абвгдеёжзийклмнопрстуфхцчшщыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯ1234567890";
        string ENG = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        string lang = "";
        long[] nums = new long[] { 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997 };

        BigInteger Gcd(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (b < a)
            {
                var t = a;
                a = b;
                b = t;
            }

            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            BigInteger gcd = Gcd(b % a, a, out x, out y);

            BigInteger newY = x;
            BigInteger newX = y - (b / a) * x;

            x = newX;
            y = newY;
            return gcd;
        }

        BigInteger calc(BigInteger a, BigInteger x, BigInteger p) // (a^x) mod p
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                var asd = Convert.ToUInt32(bit[i]);
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }

        bool testSimple(long num) // проверка числа на простоту
        {
            for (long i = 2; i <= Math.Sqrt(num); i++)
                if (num % i == 0)
                    return false;
            return true;
        }

        string encrypt(string message, BigInteger E, BigInteger n) // шифрование
        {
            string result = "";
            for (int i = 0; i < message.Length; i++)
            {
                BigInteger lit = lang.IndexOf(message[i]);
                var cal = calc(lit, E, n);
                //lit = BigInteger.Pow(lit, (int)E) % n;
                result += cal.ToString() + " ";
            }
            return result.Substring(0, result.Length - 1);
        }

        private void button1_Click(object sender, EventArgs e) // генератор вх-х данных
        {
            Random rnd = new Random();

            long p = nums[rnd.Next(0, nums.Count() - 1)];
            long q;
            do
                q = nums[rnd.Next(0, nums.Count() - 1)];
            while (q == p);

            long m = (p - 1) * (q - 1);

            textBox1.Text = (p).ToString();
            textBox2.Text = (q).ToString();

            long e_;
            do
            {
                e_ = nums[rnd.Next(0, nums.Count() - 1)];
            } while (e_ > (p * q) || e_ == p || e_ == q || Gcd(e_, m, out BigInteger outX, out BigInteger outY) != 1); // генерировать e_ до тех пор пока оно больше n, не равно p и q, и НОД (e_; m) не будет равен 1

            textBox3.Text = e_.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox5.Text = "";

            lang = ENG;
            if (comboBox1.Text == "RUS")
                lang = RUS;

            long p;
            long q;
            long E;

            try
            {
                p = Convert.ToInt64(textBox1.Text);
                q = Convert.ToInt64(textBox2.Text);
                E = Convert.ToInt64(textBox3.Text);
                if (p == q || p == E || q == E)
                    throw new Exception();
            }
            catch (Exception)
            {
                MessageBox.Show("Неверный тип введённых данных!");
                return;
            }

            if (testSimple(p) == false || testSimple(q) == false || testSimple(E) == false)
            {
                MessageBox.Show("Введенные числа должны быть простыми!");
                return;
            }
            if ((p * q) < lang.Length)
            {
                MessageBox.Show("(p * q) должно быть больше " + lang.Length);
                return;
            }

            BigInteger n = p * q;
            BigInteger m = (p - 1) * (q - 1);

            if (E < 2 || E > n)
            {
                MessageBox.Show("'e' должно быть больше 1 и меньше " + n.ToString());
                return;
            }

            BigInteger d;
            if (Gcd(E, m, out BigInteger outX, out BigInteger outY) == 1)
                d = (outX % m + m) % m; // инверсия (мультипликативно обратное к числу e)
            else
            {
                MessageBox.Show("'e' должно быть взаимно простым с " + m);
                return;
            }

            if(true)
            {
                Gcd(11, 251, out BigInteger ou1tX, out BigInteger ou1tY);
                var asd = ou1tX;
            }

            string message = textBox4.Text;
            textBox5.Text = encrypt(message, E, n);

            label5.Text = "(" + E.ToString() + ", " + n.ToString() + ") - open key" + '\r' + '\n' + "(" + d.ToString() + ", " + n.ToString() + ") - sicret key";
            textBox6.Text = d.ToString();
            textBox7.Text = n.ToString();
        }

        private void button3_Click(object sender, EventArgs e) // расшифровка
        {
            textBox8.Text = "";
            lang = ENG;
            if (comboBox1.Text == "RUS")
                lang = RUS;


            BigInteger d;
            BigInteger n;
            try
            {
                d = Convert.ToInt64(textBox6.Text);
                n = Convert.ToInt64(textBox7.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Неверный тип введённых данных!");
                return;
            }

            var message = textBox5.Text.Split(' ');
            for (int i = 0; i < message.Count(); i++)
            {
                BigInteger a = new BigInteger(Convert.ToInt64(message[i]));
                if (a < 0)
                    continue;

                //a = BigInteger.Pow(a, (int)d) % n;
                a = calc(a, d, n);
                if (a > lang.Length)
                {
                    textBox8.Text = "Неверные входные данные!";
                    break;
                }
                textBox8.Text += lang[Convert.ToInt32(a.ToString())].ToString();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8 && number != 32) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }
    }
}
