﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace CMofIS12_WF_Part2
{
    public partial class Lab1 : Form
    {
        public Lab1()
        {
            InitializeComponent();
        }

        int NOD(int a, int b)
        {
            int x2 = 1; // переменные для реализации
            int x1 = 0; // расширенного
            int y2 = 0; // алгоритма Евклида
            int y1 = 1; // .


            if (a == 0 && b == 0) // проверка если оба числа равны нулю
            {
                MessageBox.Show("Оба числа равны 0!"); // вывод сообщения
                label4.Text = label5.Text = ""; // отчищаем строки вывода информации
                return 0;
            }

            int a1 = a; // сохраняем введённое число в переменную a1
            int b1 = b; // сохраняем введённое число в переменную b1

            while (b1 > 0) // реализация расширенного алгоритма евклида
            {
                int q = a1 / b1;
                int r = a1 % b1;
                a1 = b1;
                b1 = r;
                int xx = x2 - q * x1;
                int yy = y2 - q * y1;
                x2 = x1;
                x1 = xx;

                y2 = y1;
                y1 = yy;
            }
            int x = x2; // коэффициент перед первым числом
            int y = y2; // коэффициент перед вторым числом

            label5.Text = "КОЭФФИЦИЕНТЫ:" + x.ToString() + " и " + y.ToString(); // вывод коэффициентов на форму/экран
            return a1 = a * x + b * y; // НОД

        }

        BigInteger calc(int a, int x, int p)
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString(x, 2);
            for (int i = bit.Length-1; i > -1; i--)
            {
                var asd = Convert.ToUInt32(bit[i]);
                if (Convert.ToUInt32(bit[i]) -48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }

        private long inversion(long a, long p)// вычисление числа d
        {
            long d = 1;

            while (true)
            {
                if ((d * a) % p == 1)
                    break;
                else
                    d++;
            }

            return d;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label5.Text = "КОЭФФИЦИЕНТЫ: ";
            label4.Text = "НОД: ";
            int a, b;


            try
            {
                a = Convert.ToInt32(textBox1.Text); // считали первое число в переменную a
                b = Convert.ToInt32(textBox2.Text); // считали второе число в переменную b
            }
            catch
            {
                MessageBox.Show("Введите корректные данные");
                return;
            }

            if (a < 0) // проверка если первое число меньше нуля
            {
                a = a * (-1); // умножить его на -1
                textBox1.Text = a.ToString(); // вывести результат на форму/экран
            }
            if (b < 0) // проверка если второе число меньше нуля
            {
                b = b * (-1); // умножить его на -1
                textBox2.Text = b.ToString(); // вывести результат на форму/экран
            }


            label4.Text = "НОД: " + NOD(a, b).ToString(); // вывод НОД на форму/экран
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label10.Text = "ОТВЕТ: ";
            int a, x, p;
            
            try
            {
                a = Convert.ToInt32(textBox3.Text);
                x = Convert.ToInt32(textBox4.Text);
                p = Convert.ToInt32(textBox5.Text);
            }
            catch
            {
                MessageBox.Show("Введите корректные данные");
                return;
            }
            if (x < 0)
            {
                MessageBox.Show("степень должна быть положительная!");
                return;
            }


            label10.Text = "ОТВЕТ: " + calc(a, x, p).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label14.Text = "ОТВЕТ: ";

            int a, p;
            try
            {
                a = Convert.ToInt32(textBox6.Text);
                p = Convert.ToInt32(textBox7.Text);
                if (p <= 1)
                {
                    MessageBox.Show("модуль должен быть больше 1");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Введите корректные данные");
                return;
            }

            if (NOD(a, p) != 1)
            {
                MessageBox.Show("Числа 'a' и 'p' должны быть взаимно простыми");
                return;
            }

            label14.Text = "ОТВЕТ: " + inversion(a, p);
          //  label14.Text += " | " + Math.Pow(a, eiler(p) - 1) % p;

        }

        //int eiler(int n)
        //{
        //    int result = n;
        //    for (int i = 2; i * i <= n; ++i)
        //        if (n % i == 0)
        //        {
        //            while (n % i == 0)
        //                n /= i;
        //            result -= result / i;
        //        }
        //    if (n > 1)
        //        result -= result / n;
        //    return result;
        //}

    }
}
