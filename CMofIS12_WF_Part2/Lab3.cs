﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace CMofIS12_WF_Part2
{
    public partial class Lab3 : Form
    {
        public Lab3()
        {
            InitializeComponent();

        }
        //long[] nums = new long[]
        int[] simplNumber_g = new int[] { 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293 };
        int[] simplNumber_q = new int[] { 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997 };

        //int[] numberQ = new int[] { 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293 };
        //int[] naturalNumber_a_b = new int[] { 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 };

        private void inputGenerator_Click(object sender, EventArgs e) // генератор вх-х данных
        {
            Random rnd = new Random();
            long randP;
            long randQ;
            long randG;



            do
            {
                randQ = simplNumber_q[rnd.Next(0, simplNumber_q.Count() - 1)]; // генерация q
                var rrr = testSimple(randQ);
                randP = (2 * randQ) + 1;
            } while (randQ < 2 || randQ > randP - 1 || !testSimple(randP)); // 1 < q < p-1.

            do
            {
                randG = rnd.Next(21, 500);// simplNumber_g[rnd.Next(0, simplNumber_g.Count() - 1)]; // генерация g
                var rrr = calc(randG, randQ, randP);
            } while (calc(randG, randQ, randP) == 1 || randG >= randP - 1); // 1< g <р-1 и g^q mod p ≠ 1

            //var test_q = BigInteger.Pow(randQ, eiler(randP)) % randP; // g является первообразным корнем по модулю p, т.е. test_q == 1

            variable_g.Text = randG.ToString();
            variable_p.Text = randP.ToString();
            variable_q.Text = randQ.ToString();

            int rand_a = rnd.Next(21, 100);
            int rand_b = rnd.Next(21, 125);
            variable_a.Text = rand_a.ToString();
            variable_b.Text = rand_b.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelA.Text = "";
            labelB.Text = "";
            labelKA.Text = "";
            labelKB.Text = "";

            long randP;
            long randG;
            long randQ;
            long rand_a;
            long rand_b;
            try
            {
                randG = (Convert.ToInt64(variable_g.Text));
                randQ = (Convert.ToInt64(variable_q.Text));
                randP = (2 * randQ) + 1;
                variable_p.Text = randP.ToString();
                rand_a = (Convert.ToInt64(variable_a.Text));
                rand_b = (Convert.ToInt64(variable_b.Text));
            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректные данные!");
                return;
            }
            if (testSimple(randP) == false || testSimple(randQ) == false)
            {
                MessageBox.Show("числa 'p' и 'q'  должнЫ быть простыми!");
                return;
            }
            if (randQ < 2 || randQ > randP - 1)
            {
                MessageBox.Show("число 'q' должно быть (1 < q < p-1) !");
                return;
            }
            if (rand_a < 2 || rand_b < 2)
            {
                MessageBox.Show("числа 'а', 'b' должны быть > 1!");
                return;
            }
            if (calc(randG, randQ, randP) == 1 || randG >= randP - 1)
            {
                MessageBox.Show(" 1 < g < р - 1 и g^q mod p ≠ 1");
                return;
            }

            //BigInteger A = BigInteger.Pow(randQ, Convert.ToInt32(rand_a)) % randP; // вычисление A 
            BigInteger A = calc(randG, rand_a, randP); // вычисление A 

            //////////////////////// Условно: Алиса передает Бобу - A, p, q ////////////////////////

            //BigInteger B = BigInteger.Pow(randQ, Convert.ToInt32(rand_b)) % randP; // вычисление B
            BigInteger B = calc(randG, rand_b, randP);
            //BigInteger KForBob = BigInteger.Pow(A, Convert.ToInt32(rand_b)) % randP; // вычисление K - секретного ключа
            BigInteger KForBob = calc(A, rand_b, randP);

            //////////////////////// Условно: Боб передает Алисе - B ////////////////////////

            //BigInteger KForAlice = BigInteger.Pow(B, Convert.ToInt32(rand_a)) % randP; // вычисление K - секретного ключа
            BigInteger KForAlice = calc(B, rand_a, randP);

            //////////////////////// На текущий момент Алиса и Боб имеют секрктные ключи ////////////////////////

            labelA.Text = A.ToString();
            labelB.Text = B.ToString();
            labelKA.Text = KForAlice.ToString();
            labelKB.Text = KForBob.ToString();
        }

        BigInteger calc(BigInteger a, BigInteger x, BigInteger p) // (a^x) mod p
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                var asd = Convert.ToUInt32(bit[i]);
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }

        bool testSimple(long num) // проверка числа на простоту
        {
            //new BigInteger(100);
            for (long i = 2; i <= Math.Sqrt(num); i++)
                if (num % i == 0)
                    return false;
            return true;
        }

        //int eiler(int n) // функция Эйлера
        //{
        //    int result = n;
        //    for (int i = 2; i * i <= n; ++i)
        //        if (n % i == 0)
        //        {
        //            while (n % i == 0)
        //                n /= i;
        //            result -= result / i;
        //        }
        //    if (n > 1)
        //        result -= result / n;
        //    return result;
        //}

        private void help_Click(object sender, EventArgs e)
        {
            helpFormLab3 newForm = new helpFormLab3();
            newForm.Show();
        }

        private void variable_p_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void variable_q_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void variable_a_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }

        private void variable_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8) // цифры и клавиша BackSpace
            {
                e.Handled = true;
            }
        }


    }
}
