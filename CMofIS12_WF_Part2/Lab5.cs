﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace CMofIS12_WF_Part2
{
    public partial class Lab5 : Form
    {
        public Lab5()
        {
            InitializeComponent();
        }

        string lang = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        long[] simplNumber = new long[] { 9369319, 63018038201, 90825, 262419, 361275, 481899, 1354828, 6328548, 6679881, 1046527, 16769023, 1073676287, 68718952447};

        private void inputGenerator_Click(object sender, EventArgs e) // генератор вх-х данных
        {
            Random rnd = new Random();
            long randP;
            BigInteger randG;

            do
            {
                randP = simplNumber[rnd.Next(0, simplNumber.Count() - 1)]; // генерация
            } while (!testSimple(randP)); // 1 < q < p-1.

            Gcd(randP, 323, out BigInteger outX, out BigInteger outY);
            randG = (outX % randP + randP) % randP; // инверсия (мультипликативно обратное к числу randP)

            if (calc(randG, eiler(randP), randP) != 1)
                MessageBox.Show("error");

            long rand_c = 0;
            BigInteger rand_d = 0;
            do
            {
                rand_c = rnd.Next(10, 1000);
                rand_d = calc(randG, rand_c, randP);
            } while (rand_c > randP - 1);
            variable_cAdA.Text = rand_c.ToString() + ";" + rand_d.ToString();

            do
            {
                rand_c = rnd.Next(10, 1000);
                rand_d = calc(randG, rand_c, randP);
            } while (rand_c > randP - 1);


            variable_p.Text = randP.ToString();
            variable_g.Text = randG.ToString();
            pShip.Text = "---------------------->       " + randP.ToString();
            gShip.Text = "---------------------->       " + randG.ToString();
            variable_cBdB.Text = rand_c.ToString() + ";" + rand_d.ToString();
            //variable_cBdB =

        }
        private void begin_Click(object sender, EventArgs e)
        {
            labelK.Text = "___";
            lang = textLang.Text;
            Random rnd = new Random();

            long p;
            long g;
            long cA;
            BigInteger dA;
            long cB;
            BigInteger dB;
            try
            {
                p = Convert.ToInt64(variable_p.Text);
                g = Convert.ToInt64(variable_g.Text);

            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректные данные!");
                return;
            }
            if(p < lang.Length-1)
            {
                MessageBox.Show("p > " + lang.Length);
                return;
            }
            if (testSimple(p) == false)
            {
                MessageBox.Show("Число p должно быть простым!");
                return;
            }
            if (calc(g, eiler(p), p) != 1)
            {
                MessageBox.Show("число g должно быть первообразным корнем числа p!");
                return;
            }

            try
            {

                string[] massA = variable_cAdA.Text.Split(';');
                string[] massB = variable_cBdB.Text.Split(';');
                cA = Convert.ToInt64(massA[0]);
                dA = Convert.ToInt64(massA[1]);
                cB = Convert.ToInt64(massB[0]);
                dB = Convert.ToInt64(massB[1]);

            }
            catch (Exception)
            {
                do
                {
                    cA = rnd.Next(10, 90825);
                    dA = calc(g, cA, p);
                } while (cA > p - 1);
                variable_cAdA.Text = cA.ToString() + ";" + dA.ToString();

                do
                {
                    cB = rnd.Next(10, 90825);
                    dB = calc(g, cB, p);
                } while (cB > p - 1);
                variable_cBdB.Text = cB.ToString() + ";" + dB.ToString();
                MessageBox.Show("Значения (cA;dA) и (cB;dB) небыли введены или введены некорректно! \r \n Эти значения будут сгенерированы.");
            }

            int k = rnd.Next(2, Math.Abs((int)p - 2)); // ?????????????
            labelK.Text = "Сессионный ключ: " + k.ToString();
            //////// шифр сообщения
            encryptText.Text = encrypt(message.Text, g, p, k, dB);

        }
        private void take_Click(object sender, EventArgs e)
        {
            long p;
            long cB;

            try
            {
                string[] massB = variable_cBdB.Text.Split(';');
                cB = Convert.ToInt64(massB[0]);
                p = Convert.ToInt64(variable_p.Text);

            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректные данные!");
                return;
            }

            textResult.Text = decrypt(encryptText.Text, p, cB);
        }
        string encrypt(string message, BigInteger g, BigInteger p, int k, BigInteger dB) // шифрование
        {
           
            var r = calc(g, k, p);
            //var e = calc();

            string result = "";
            for (int i = 0; i < message.Length; i++)
            {
                BigInteger lit = lang.IndexOf(message[i]);
                var e = calc(lit*calc(dB,k, p), 1, p);
                result += "("+r.ToString() + ";"+e.ToString()+")";
            }
            return result;//.Substring(0, result.Length - 1);
        }
        string decrypt(string message, BigInteger p, BigInteger cB)
        {
            string res = "";
            var splitText = message.Split(new char[] { '(', ')' }, StringSplitOptions.RemoveEmptyEntries );
            for (int i = 0; i < splitText.Length; i++)
            {
                var split = splitText[i].Split(';');


                var time = calc(BigInteger.Parse(split[1]) * calc(BigInteger.Parse(split[0]), p - 1 - cB, p), 1,p);
                if (time%p > lang.Length)
                {
                    res = "Неверные входные данные!";
                    break;
                }
                if (time < 0) continue;
                res += lang[Convert.ToInt32(time.ToString())].ToString();
            }
            return res;
        }
        bool testSimple(long num) // проверка числа на простоту
        {
            //new BigInteger(100);
            for (long i = 2; i <= Math.Sqrt(num); i++)
                if (num % i == 0)
                    return false;
            return true;
        }
        long eiler(long n) // функция Эйлера
        {
            long result = n;
            for (long i = 2; i * i <= n; ++i)
                if (n % i == 0)
                {
                    while (n % i == 0)
                        n /= i;
                    result -= result / i;
                }
            if (n > 1)
                result -= result / n;
            return result;
        }
        ulong Eyler(ulong n)
        {
            ulong res = n, en = Convert.ToUInt64(Math.Sqrt(n) + 1);
            for (ulong i = 2; i <= en; i++)
                if ((n % i) == 0)
                {
                    while ((n % i) == 0)
                        n /= i;
                    res -= (res / i);
                }
            if (n > 1) res -= (res / n);
            return res;
        }

        BigInteger calc(BigInteger a, BigInteger x, BigInteger p) // (a^x) mod p
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }
        BigInteger calc(BigInteger a, BigInteger x) // (a^x)
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                {
                    y = (y * s);
                }
                s = (s * s);
            }
            return y;
        }

        BigInteger Gcd(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (b < a)
            {
                var t = a;
                a = b;
                b = t;
            }

            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            BigInteger gcd = Gcd(b % a, a, out x, out y);

            BigInteger newY = x;
            BigInteger newX = y - (b / a) * x;

            x = newX;
            y = newY;
            return gcd;
        }


    }
}
