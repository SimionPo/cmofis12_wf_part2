﻿namespace CMofIS12_WF_Part2
{
    partial class Lab10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.variable_g = new System.Windows.Forms.TextBox();
            this.variable_p = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.inputGenerator = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.variable_xy = new System.Windows.Forms.TextBox();
            this.begin = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.messageCode = new System.Windows.Forms.TextBox();
            this.buttonTest = new System.Windows.Forms.Button();
            this.textBoxTest = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "g:";
            // 
            // variable_g
            // 
            this.variable_g.Location = new System.Drawing.Point(35, 63);
            this.variable_g.Name = "variable_g";
            this.variable_g.Size = new System.Drawing.Size(119, 22);
            this.variable_g.TabIndex = 13;
            // 
            // variable_p
            // 
            this.variable_p.Location = new System.Drawing.Point(35, 34);
            this.variable_p.Name = "variable_p";
            this.variable_p.Size = new System.Drawing.Size(119, 22);
            this.variable_p.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "p:";
            // 
            // inputGenerator
            // 
            this.inputGenerator.BackColor = System.Drawing.Color.GhostWhite;
            this.inputGenerator.Location = new System.Drawing.Point(12, 105);
            this.inputGenerator.Name = "inputGenerator";
            this.inputGenerator.Size = new System.Drawing.Size(498, 23);
            this.inputGenerator.TabIndex = 15;
            this.inputGenerator.Text = "Сгенерировать входные данные!";
            this.inputGenerator.UseVisualStyleBackColor = false;
            this.inputGenerator.Click += new System.EventHandler(this.inputGenerator_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Message:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "Code:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "Test:";
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(81, 144);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(429, 22);
            this.message.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(162, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 17);
            this.label4.TabIndex = 28;
            this.label4.Text = "x;y:";
            // 
            // variable_xy
            // 
            this.variable_xy.Location = new System.Drawing.Point(197, 63);
            this.variable_xy.Name = "variable_xy";
            this.variable_xy.Size = new System.Drawing.Size(135, 22);
            this.variable_xy.TabIndex = 27;
            // 
            // begin
            // 
            this.begin.BackColor = System.Drawing.Color.GhostWhite;
            this.begin.Location = new System.Drawing.Point(423, 243);
            this.begin.Name = "begin";
            this.begin.Size = new System.Drawing.Size(75, 26);
            this.begin.TabIndex = 29;
            this.begin.Text = "begin";
            this.begin.UseVisualStyleBackColor = false;
            this.begin.Click += new System.EventHandler(this.begin_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(194, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 17);
            this.label5.TabIndex = 30;
            this.label5.Text = "sicret key ; open key";
            // 
            // messageCode
            // 
            this.messageCode.Location = new System.Drawing.Point(81, 173);
            this.messageCode.Name = "messageCode";
            this.messageCode.Size = new System.Drawing.Size(429, 22);
            this.messageCode.TabIndex = 31;
            // 
            // buttonTest
            // 
            this.buttonTest.BackColor = System.Drawing.Color.GhostWhite;
            this.buttonTest.Location = new System.Drawing.Point(423, 275);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 26);
            this.buttonTest.TabIndex = 32;
            this.buttonTest.Text = "test";
            this.buttonTest.UseVisualStyleBackColor = false;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // textBoxTest
            // 
            this.textBoxTest.Location = new System.Drawing.Point(81, 204);
            this.textBoxTest.Name = "textBoxTest";
            this.textBoxTest.Size = new System.Drawing.Size(429, 22);
            this.textBoxTest.TabIndex = 33;
            // 
            // Lab10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 433);
            this.Controls.Add(this.textBoxTest);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.messageCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.begin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.variable_xy);
            this.Controls.Add(this.message);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.inputGenerator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.variable_g);
            this.Controls.Add(this.variable_p);
            this.Controls.Add(this.label1);
            this.Name = "Lab10";
            this.Text = "Электронная подпись на базе шифра Эль-Гамаля";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox variable_g;
        private System.Windows.Forms.TextBox variable_p;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button inputGenerator;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox variable_xy;
        private System.Windows.Forms.Button begin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox messageCode;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.TextBox textBoxTest;
    }
}