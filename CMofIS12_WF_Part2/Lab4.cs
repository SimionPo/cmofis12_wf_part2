﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace CMofIS12_WF_Part2
{
    public partial class Lab4 : Form
    {
        public Lab4()
        {
            InitializeComponent();
        }

        string lang = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        long[] simplNumber = new long[] { 65713, 69313, 73009, 76801, 84673, 16769023, 1073676287, 68718952447, 274876858367, 4398042316799 };//{ 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997 };

        private void inputGenerator_Click(object sender, EventArgs e) // генератор вх-х данных
        {
            Random rnd = new Random();
            long randP;

            do
            {
                randP = simplNumber[rnd.Next(0, simplNumber.Count() - 1)]; // генерация q
            } while (!testSimple(randP)); // 1 < q < p-1.


            variable_p1.Text = variable_p.Text = randP.ToString();

            long rand_a = 0;
            long rand_b = 0;

            do
            {
                rand_a = rnd.Next(9999, 999999999);
                rand_b = rnd.Next(17000, 898212112);
                if (rand_a % 2 == 0 || rand_b % 2 == 0)
                    continue;
            } while (calc(rand_a * rand_b, 1, randP - 1) != 1);
            variable_c.Text = rand_a.ToString() + ";" + rand_b.ToString();

            do
            {
                rand_a = rnd.Next(9999, 999999999);
                rand_b = rnd.Next(17000, 898212112);
                if(rand_a % 2 == 0 || rand_b % 2 == 0)
                    continue;
            } while (calc(rand_a * rand_b, 1, randP - 1) != 1);
            variable_d.Text = rand_a.ToString() + ";" + rand_b.ToString();
        }

        private void Begin_Click(object sender, EventArgs e)
        {
            lang = textLang.Text;
            long p;
            long ca;
            long cb;
            long da;
            long db;

            try
            {
                string[] massC = variable_c.Text.Split(';');
                string[] massD = variable_d.Text.Split(';');
                p = Convert.ToInt64(variable_p.Text);
                ca = Convert.ToInt64(massC[0]);
                cb = Convert.ToInt64(massC[1]);
                da = Convert.ToInt64(massD[0]);
                db = Convert.ToInt64(massD[1]);
            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректные данные!");
                return;
            }
            if(testSimple(p)== false)
            {
                MessageBox.Show("Число p должно быть простым!");
                return;
            }
            if(calc(ca * cb, 1, p - 1) != 1)
            {
                MessageBox.Show("Не выполнено условие (cA * cB mod p-1 == 1)!");
                return;
            }
            if (calc(da * db, 1, p - 1) != 1)
            {
                MessageBox.Show("Не выполнено условие (dA * dB mod p-1 == 1)!");
                return;
            }

            textBox5.Text = encrypt(message.Text, ca, p);
            A.Text = "ALICE, (message^cA mod p) --> BOB";
            textBox6.Text = up(textBox5.Text.Split(' '), da, p);

            textBox7.Text = up(textBox6.Text.Split(' '), cb, p);

            textBox8.Text = decrypt(textBox7.Text.Split(' '), db, p);

         

            //var rrr = Encoding.Default.GetBytes(message.Text);
            //var rrrr = (calc((int)rrr[0], ca, p));
            //rrr[0] = (byte)295; //(byte)(calc((int)rrr[0], ca, p));
            //textBox5.Text = System.Text.Encoding.Default.GetString(rrr);

        }

        bool testSimple(long num) // проверка числа на простоту
        {
            //new BigInteger(100);
            for (long i = 2; i <= Math.Sqrt(num); i++)
                if (num % i == 0)
                    return false;
            return true;
        }
        BigInteger calc(BigInteger a, BigInteger x, BigInteger p) // (a^x) mod p
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                var asd = Convert.ToUInt32(bit[i]);
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }
        string encrypt(string message, BigInteger E, BigInteger n) // шифрование
        {
            string result = "";
            for (int i = 0; i < message.Length; i++)
            {
                BigInteger lit = lang.IndexOf(message[i]);
                BigInteger cal = calc(lit, E, n);
                result += cal.ToString() + " ";
            }
            return result.Substring(0, result.Length - 1);
        }

        string up(string[] message, BigInteger m, BigInteger mod)
        {
            string res="";
            for (int i = 0; i < message.Count(); i++)
            {
                BigInteger a = BigInteger.Parse(message[i]);
                if (a < 0)
                    continue;
                if (a > mod)
                    MessageBox.Show("message > mod");
                a = calc(a, m, mod);
                res += a + " ";
            }
            return res.Substring(0, res.Length - 1);
        }

        string decrypt(string[] message, BigInteger m, BigInteger mod)
        {
            string res = "";
            for (int i = 0; i < message.Count(); i++)
            {
                BigInteger a = BigInteger.Parse(message[i]);
                if (a < 0)
                    continue;
                if (a > mod)
                    MessageBox.Show("message > mod");
                a = calc(a, m, mod);
                if (a > lang.Length)
                {
                    textBox8.Text = "Неверные входные данные!";
                    break;
                }
                res += lang[Convert.ToInt32(a.ToString())].ToString();
            }
            return res;
        }
    }
}
