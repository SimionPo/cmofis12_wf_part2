﻿namespace CMofIS12_WF_Part2
{
    partial class Lab4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputGenerator = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.variable_p = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.A = new System.Windows.Forms.Label();
            this.variable_c = new System.Windows.Forms.TextBox();
            this.variable_d = new System.Windows.Forms.TextBox();
            this.message = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.Begin = new System.Windows.Forms.Button();
            this.Alice = new System.Windows.Forms.Label();
            this.Bob = new System.Windows.Forms.Label();
            this.variable_p1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textLang = new System.Windows.Forms.TextBox();
            this.B = new System.Windows.Forms.Label();
            this.C = new System.Windows.Forms.Label();
            this.D = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // inputGenerator
            // 
            this.inputGenerator.BackColor = System.Drawing.Color.Ivory;
            this.inputGenerator.Location = new System.Drawing.Point(3, 182);
            this.inputGenerator.Name = "inputGenerator";
            this.inputGenerator.Size = new System.Drawing.Size(385, 23);
            this.inputGenerator.TabIndex = 0;
            this.inputGenerator.Text = "Сгенерировать входные данные!";
            this.inputGenerator.UseVisualStyleBackColor = false;
            this.inputGenerator.Click += new System.EventHandler(this.inputGenerator_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "p:";
            // 
            // variable_p
            // 
            this.variable_p.Location = new System.Drawing.Point(54, 121);
            this.variable_p.Name = "variable_p";
            this.variable_p.Size = new System.Drawing.Size(100, 22);
            this.variable_p.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "cA;cB:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "dA;dB:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Message:";
            // 
            // A
            // 
            this.A.AutoSize = true;
            this.A.Location = new System.Drawing.Point(12, 250);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(267, 17);
            this.A.TabIndex = 6;
            this.A.Text = "ALICE, (A = message^cA mod p) --> BOB";
            // 
            // variable_c
            // 
            this.variable_c.Location = new System.Drawing.Point(54, 151);
            this.variable_c.Name = "variable_c";
            this.variable_c.Size = new System.Drawing.Size(100, 22);
            this.variable_c.TabIndex = 7;
            // 
            // variable_d
            // 
            this.variable_d.Location = new System.Drawing.Point(288, 151);
            this.variable_d.Name = "variable_d";
            this.variable_d.Size = new System.Drawing.Size(100, 22);
            this.variable_d.TabIndex = 8;
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(75, 216);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(313, 22);
            this.message.TabIndex = 9;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(3, 273);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(384, 22);
            this.textBox5.TabIndex = 10;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(3, 318);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(385, 22);
            this.textBox6.TabIndex = 11;
            // 
            // Begin
            // 
            this.Begin.BackColor = System.Drawing.Color.Ivory;
            this.Begin.Location = new System.Drawing.Point(312, 244);
            this.Begin.Name = "Begin";
            this.Begin.Size = new System.Drawing.Size(75, 23);
            this.Begin.TabIndex = 12;
            this.Begin.Text = "Begin";
            this.Begin.UseVisualStyleBackColor = false;
            this.Begin.Click += new System.EventHandler(this.Begin_Click);
            // 
            // Alice
            // 
            this.Alice.AutoSize = true;
            this.Alice.Location = new System.Drawing.Point(72, 77);
            this.Alice.Name = "Alice";
            this.Alice.Size = new System.Drawing.Size(46, 17);
            this.Alice.TabIndex = 13;
            this.Alice.Text = "ALICE";
            // 
            // Bob
            // 
            this.Bob.AutoSize = true;
            this.Bob.Location = new System.Drawing.Point(320, 77);
            this.Bob.Name = "Bob";
            this.Bob.Size = new System.Drawing.Size(37, 17);
            this.Bob.TabIndex = 14;
            this.Bob.Text = "BOB";
            // 
            // variable_p1
            // 
            this.variable_p1.Enabled = false;
            this.variable_p1.Location = new System.Drawing.Point(288, 121);
            this.variable_p1.Name = "variable_p1";
            this.variable_p1.Size = new System.Drawing.Size(100, 22);
            this.variable_p1.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(232, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "p:";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(198, 77);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(11, 102);
            this.label.TabIndex = 17;
            this.label.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(3, 364);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(385, 22);
            this.textBox7.TabIndex = 18;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(3, 411);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(384, 22);
            this.textBox8.TabIndex = 19;
            // 
            // textLang
            // 
            this.textLang.Location = new System.Drawing.Point(3, 3);
            this.textLang.Multiline = true;
            this.textLang.Name = "textLang";
            this.textLang.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textLang.Size = new System.Drawing.Size(385, 71);
            this.textLang.TabIndex = 20;
            this.textLang.Text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ";
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Location = new System.Drawing.Point(12, 298);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(220, 17);
            this.B.TabIndex = 21;
            this.B.Text = "BOB, (B = A^dA mod p) --> ALICE";
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Location = new System.Drawing.Point(13, 343);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(219, 17);
            this.C.TabIndex = 22;
            this.C.Text = "ALICE, (C = B^cB mod p) --> BOB";
            // 
            // D
            // 
            this.D.AutoSize = true;
            this.D.Location = new System.Drawing.Point(12, 389);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(238, 17);
            this.D.TabIndex = 23;
            this.D.Text = "BOB, (D = C^dB mod p) = DECRYPT";
            // 
            // Lab4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 445);
            this.Controls.Add(this.D);
            this.Controls.Add(this.C);
            this.Controls.Add(this.B);
            this.Controls.Add(this.textLang);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label);
            this.Controls.Add(this.variable_p1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Bob);
            this.Controls.Add(this.Alice);
            this.Controls.Add(this.Begin);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.message);
            this.Controls.Add(this.variable_d);
            this.Controls.Add(this.variable_c);
            this.Controls.Add(this.A);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.variable_p);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputGenerator);
            this.Name = "Lab4";
            this.Text = "Шифр Шамира";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button inputGenerator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox variable_p;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label A;
        private System.Windows.Forms.TextBox variable_c;
        private System.Windows.Forms.TextBox variable_d;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button Begin;
        private System.Windows.Forms.Label Alice;
        private System.Windows.Forms.Label Bob;
        private System.Windows.Forms.TextBox variable_p1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textLang;
        private System.Windows.Forms.Label B;
        private System.Windows.Forms.Label C;
        private System.Windows.Forms.Label D;
    }
}