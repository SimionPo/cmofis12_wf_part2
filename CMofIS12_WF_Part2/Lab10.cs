﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Security.Cryptography;

namespace CMofIS12_WF_Part2
{
    public partial class Lab10 : Form
    {
        public Lab10()
        {
            InitializeComponent();
        }
        long[] nums = new long[] { 65713, 69313, 73009, 76801, 84673, 16769023, 1073676287, 68718952447, 274876858367, 4398042316799 };
        private void inputGenerator_Click(object sender, EventArgs e) // подбор входных данных
        {
            Random rnd = new Random();
            long randP;
            BigInteger randG;

            do
            {
                randP = nums[rnd.Next(0, nums.Count() - 1)]; // генерация
            } while (!testSimple(randP)); // 1 < q < p-1.

            Gcd(randP, 323, out BigInteger outX, out BigInteger outY);
            randG = (outX % randP + randP) % randP; // инверсия (мультипликативно обратное к числу randP)

            variable_p.Text = randP.ToString();
            variable_g.Text = randG.ToString();
        }

        private void begin_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();

            long p;
            long g;
            long x; // sicret key
            BigInteger y; // open key
            long k; // случайно число k ( 1< k < р-1)
            BigInteger r; // r = g^k mod p.
            BigInteger OutX; // коэффициенты алгоритма Евклида
            BigInteger OutY;
            BigInteger u; //  u = (h - xr) mod(р - 1),
            BigInteger s; // s = (k^-1) u mod(p-1).

            try
            {
                p = Convert.ToInt64(variable_p.Text);
                g = Convert.ToInt64(variable_g.Text);

            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректные данные!");
                return;
            }
            if (testSimple(p) == false)
            {
                MessageBox.Show("Число p должно быть простым!");
                return;
            }
            if (calc(g, eiler(p), p) != 1)
            {
                MessageBox.Show("Число g должно быть первообразным корнем числа p!");
                return;
            }

            // вычисление x и y
            do
            {
                x = rnd.Next(10, 90825);
                y = calc(g, x, p);
            } while (x >= p - 1);
            //x = 7;//////////////////////////////////////////////////////////////////////////////// - тестовые значения
            //y = 17;////////////////////////////////////////////////////////////////////////////////
            variable_xy.Text = x.ToString() + ";" + y.ToString();

            // получаем хеш и проверяем правило 1<h<р
            var hash = GetHash(message.Text);
            //hash = 3;////////////////////////////////////////////////////////////////////////////////
            if (hash > p) MessageBox.Show("Условие (1 < hash < р) - не выполнено."); // стоит ли тогда принять: hash = hash % p
            if (hash < 0) hash += p;

            // получаем случайное k взаимно простое с p-1
            // и r = g^k mod p.
            do
            {
                k = rnd.Next(50, 90820);
            } while (k >= p - 1 || Gcd(k, p - 1, out OutX, out OutY) != 1);
            //k = 5;////////////////////////////////////////////////////////////////////////////////
            r = calc(g, k, p);

            // вычислим u = (h - xr) mod(р - 1),
            // s = (k^-1) u mod(p-1).
            u = (hash - x * r) % (p - 1);
            if (u < 0) u += p - 1;
            Gcd(p - 1, k, out OutX, out OutY); // OutX = k^-1
            s = (OutX * u) % (p - 1); // s = (k^-1) u mod(p-1).
            if (s < 0) s += p - 1;

            messageCode.Text = message.Text + ";" + r + ";" + s;
        }
        private void buttonTest_Click(object sender, EventArgs e)
        {
            long r; //  
            long s; // 
            long p; // 
            long y; // 
            long g; // 
            // получаем хеш
            var hash = GetHash(message.Text);
            //hash = 3;////////////////////////////////////////////////////////////////////////////////
            try
            {
                string[] massA = messageCode.Text.Split(';');
                string[] massB = variable_xy.Text.Split(';');
                r = Convert.ToInt64(massA[1]);
                s = Convert.ToInt64(massA[2]);
                p = Convert.ToInt64(variable_p.Text);
                y = Convert.ToInt64(massB[1]);
                g = Convert.ToInt64(variable_g.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Введите корректные данные!");
                return;
            }

            var y_ = calc(y, r, p);
            var r_ = calc(r, s, p);
            var yr = calc(y_ * r_, 1, p ); // y^r * r^s mod p

            var gg = calc(g, hash, p); // g^h mod p

            textBoxTest.Text = "y^r * r^s = " + yr + "     g^h mod p = " + gg;
        }
        public BigInteger GetHash(string input)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            BigInteger result = 0;
            foreach (var item in hash)
            {
                result += item;
            }
            return result;//BitConverter.ToString(hash).Replace("-", ""); ;
        }
        BigInteger calc(BigInteger a, BigInteger x, BigInteger p) // (a^x) mod p
        {
            BigInteger y = 1;
            BigInteger s = a;
            var bit = Convert.ToString((long)x, 2);
            for (int i = bit.Length - 1; i > -1; i--)
            {
                var asd = Convert.ToUInt32(bit[i]);
                if (Convert.ToUInt32(bit[i]) - 48 == 1)
                    y = (y * s) % p;
                s = (s * s) % p;
            }
            return y;
        }
        BigInteger Gcd(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (b < a)
            {
                var t = a;
                a = b;
                b = t;
            }

            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            BigInteger gcd = Gcd(b % a, a, out x, out y);

            BigInteger newY = x;
            BigInteger newX = y - (b / a) * x;

            x = newX;
            y = newY;
            return gcd;
        }
        bool testSimple(long num) // проверка числа на простоту
        {
            for (long i = 2; i <= Math.Sqrt(num); i++)
                if (num % i == 0)
                    return false;
            return true;
        }
        long eiler(long n) // функция Эйлера
        {
            long result = n;
            for (long i = 2; i * i <= n; ++i)
                if (n % i == 0)
                {
                    while (n % i == 0)
                        n /= i;
                    result -= result / i;
                }
            if (n > 1)
                result -= result / n;
            return result;
        }

    }
}
