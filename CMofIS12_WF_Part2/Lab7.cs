﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMofIS12_WF_Part2
{
    public partial class Lab7 : Form
    {
        public int sizeOfBlock { get; set; } = 512; // размер блока 512 бит
        public int sizeOfWord { get; set; } = 32; // размер слова
        private const int sizeOfChar = 8; // размер одного символа (in Unicode 16 bit)
        string[] blocks; // сами блоки в двоичном формате
        // инициализация буфера
        string A, B, C, D, E, a_, b_, c_, d_, e_ = "";
        private void Initialization()
        {
            A = "01100111010001010010001100000001".Replace(" ", ""); // 67452301
            B = "11101111110011011010101110001001".Replace(" ", ""); // ЕFСDАВ89
            C = "10011000101110101101110011111110".Replace(" ", ""); // 98ВАDСFЕ
            D = "00010000001100100101010001110110".Replace(" ", ""); // 10325476
            E = "11000011110100101110000111110000".Replace(" ", ""); // C3D2E1F0
        }
        public Lab7()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Initialization();
            byte[] input = Encoding.UTF8.GetBytes(textBox1.Text);
            string inputBits = "";
            for (int i = 0; i < input.Count(); i++)
                inputBits += ByteToBinaryFormat(input[i]);
            // выравнивание потока
            inputBits = AppendPaddingBits(inputBits); // добавить биты дополнения
            // добавление длины сообщения
            inputBits = AppendLength(inputBits, input.Count() * 8);
            CutBitIntoBlocks(inputBits); // разбиение строки бит на блоки
            // инициализация буфера

            // вычисление в цикле
            for (int i = 0; i < blocks.Count(); i++)
            {
                a_ = A;
                b_ = B;
                c_ = C;
                d_ = D;
                e_ = E;

                string[] w = CutBlock(blocks[i]); // массив из 80 слов по 32 бита.
                var time = "";
                var k = "";
                // 4 цикла
                for (int j = 0; j < 80; j++)
                {
                    if (j >= 0 && j <= 19)
                    {
                        time = F1(b_, c_, d_);
                        k = "01011010100000100111100110011001"; // 5A827999
                    }
                    else if (j >= 20 && j <= 39)
                    {
                        time = F2(b_, c_, d_);
                        k = "01101110110110011110101110100001"; // 6ED9EBA1
                    }
                    else if (j >= 40 && j <= 59)
                    {
                        time = F3(b_, c_, d_);
                        k = "10001111000110111011110011011100"; // 8F1BBCDC
                    }
                    else if (j >= 60 && j <= 79)
                    {
                        time = F2(b_, c_, d_);
                        k = "11001010011000101100000111010110"; // CA62C1D6
                    }
                    time = f(ShiftLeft(a_, 5), time, 32);
                    time = f(e_, time, 32);
                    time = f(k, time, 32);
                    time = f(w[j], time, 32);
                    e_ = d_;
                    d_ = c_;
                    c_ = ShiftLeft(b_, 30);
                    b_ = a_;
                    a_ = time;

                }
                // Добавляем хеш-значение этой части к результату:
                A = f(a_, A, 32);
                B = f(b_, B, 32);
                C = f(c_, C, 32);
                D = f(d_, D, 32);
                E = f(e_, E, 32);
            }
            // результат вычислений
            byte[] result = new byte[0];
            result = StringFromBinaryToNormalFormat(result, A + B + C + D + E);
            textBox2.Text = BitConverter.ToString(result).Replace("-", "");
        }
        // перевод байта в двоичный формат
        private string ByteToBinaryFormat(byte input)
        {
            string output = "";

            string char_binary = Convert.ToString(input, 2);

            while (char_binary.Length < sizeOfChar)
                char_binary = "0" + char_binary;

            output += char_binary;
            return output;
        }
        // добавление нулевых бит чтобы новая длина потока стала сравнима с 448 по модулю 512
        private string AppendPaddingBits(string input)
        {
            input += "1"; // сначала к концу потока дописывают единичный бит.
            while ((input.Length % 512) != 448) // затем добавляют некоторое число нулевых бит
            {
                input += "0";
            }
            return input;
        }
        // добавление длины сообщения
        private string AppendLength(string input, int count)
        {
            string countToBit = Convert.ToString(count, 2);
            while (countToBit.Length < 64) countToBit = "0" + countToBit; // добавляем нули, если длинна меньше 64бит

            if (countToBit.Length == 64) return input + countToBit;// входящее сообщение + кол-во бит в исходном сообщении
            return "Что то пошло не так";
        }
        // разбиение обычной строки на блоки
        private void CutBitIntoBlocks(string input)
        {
            blocks = new string[input.Length / sizeOfBlock];
            for (int i = 0; i < blocks.Count(); i++)
            {
                blocks[i] = input.Substring(0, sizeOfBlock);
                input = input.Substring(sizeOfBlock);
            }
        }
        // разбиение блока на 32х разрядные слова
        private string[] CutBlock(string input)
        {
            // разбиение блока на 32х разрядные слова (16 слов)
            string[] block = new string[input.Length / sizeOfWord];
            for (int i = 0; i < block.Count(); i++)
            {
                block[i] = input.Substring(0, sizeOfWord);
                input = input.Substring(sizeOfWord);
            }
            // получение из 16 слов - 80 слов
            string[] w = new string[80];
            block.CopyTo(w, 0);
            for (int i = block.Count(); i < 80; i++)
            {
                w[i] = XOR(w[i - 3], w[i - 8]);
                w[i] = XOR(w[i], w[i - 14]);
                w[i] = ShiftLeft(XOR(w[i], w[i - 16]), 1);// циклический сдвиг влево 1
            }
            return w;
        }
        // сложение по модулю 2^32
        private string f(string s1, string s2, int mod)
        {
            string result = "";
            int time = 0;
            for (int i = s1.Length - 1; i > -1; i--)
            {
                int a = s1[i] - 48;
                int b = s2[i] - 48;
                if (a + b + time == 0)
                {
                    time = 0;
                    result = "0" + result;
                }
                else if (a + b + time == 1)
                {
                    time = 0;
                    result = "1" + result;
                }
                else if (a + b + time == 2)
                {
                    time = 1;
                    result = "0" + result;
                }
                else if (a + b + time == 3)
                {
                    time = 1;
                    result = "1" + result;
                }
            }
            if (result.Substring(result.Length - mod).Length == mod)
                return result.Substring(result.Length - mod);
            else return "error";
        }
        // циклический сдвиг влево на n эл-в
        private string ShiftLeft(string word, int n)
        {
            return word.Substring(n % word.Length) + word.Substring(0, n % word.Length);
        }
        // переводим строку с двоичными данными в байтовый массив
        private byte[] StringFromBinaryToNormalFormat(byte[] result, string input)
        {
            while (input.Length > 0)
            {
                string char_binary = input.Substring(0, sizeOfChar);
                input = input.Remove(0, sizeOfChar);

                int a = 0;
                int degree = char_binary.Length - 1;

                foreach (char c in char_binary)
                    a += Convert.ToInt32(c.ToString()) * (int)Math.Pow(2, degree--);

                Array.Resize(ref result, result.Count() + 1);// изменяем размер
                result[result.Count() - 1] = (byte)a;
            }

            return result;
        }
        // логические функции
        string F1(string B, string C, string D)
        {
            return OR(AND(B, C), AND(NOT(B), D)); //
        }
        string F2(string B, string C, string D)
        {
            return XOR(XOR(B, C), D);
        }
        string F3(string B, string C, string D)
        {
            var time = OR(AND(B, C), AND(B, D));
            return OR(time, AND(C, D));
        }
        //XOR двух строк с двоичными данными
        private string XOR(string s1, string s2)
        {
            string result = "";

            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a ^ b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        //AND двух строк с двоичными данными
        private string AND(string s1, string s2)
        {
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a & b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        //OR двух строк с двоичными данными
        private string OR(string s1, string s2)
        {
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a | b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }
        //NOT строки
        private string NOT(string s1)
        {
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                if (s1[i] == '1') result += "0";
                if (s1[i] == '0') result += "1";
            }
            return result;
        }
    }
}
