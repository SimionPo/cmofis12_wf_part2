﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CMofIS12_WF_Part2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Lab1 newForm = new Lab1();
            newForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Lab2 newForm = new Lab2();
            newForm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Lab3 newForm = new Lab3();
            newForm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Lab4 newForm = new Lab4();
            newForm.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Lab5 newForm = new Lab5();
            newForm.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Lab6 newForm = new Lab6();
            newForm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Lab7 newForm = new Lab7();
            newForm.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Lab8 newForm = new Lab8();
            newForm.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Lab9 newForm = new Lab9();
            newForm.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Lab10 newForm = new Lab10();
            newForm.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Lab11 newForm = new Lab11();
            newForm.Show();
        }
    }
}
